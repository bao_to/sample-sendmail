class Locators:
    email_field_locator = 'input[type="email"]'
    pass_field_locator = 'password'
    next_btn1 = 'VfPpkd-LgbsSe'
    next_btn2 = 'VfPpkd-LgbsSe'
    compose_button = '.T-I.T-I-KE.L3'
    send_to_field = 'to'
    subject_field = 'subjectbox'
    mail_body ='.Am.Al.editable.LW-avf.tS-tW'
    send_btn = '.T-I.J-J5-Ji.aoO.v7.T-I-atl.L3'
    mail_message = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    