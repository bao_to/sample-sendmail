import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from config import Config
import time
import pandas as pd
from locators import Locators
class sendmailtest(unittest.TestCase):
      
        @classmethod
        def testSendMail(self):
            try:
             
                options = webdriver.ChromeOptions()
                options.add_argument("start-maximized")
                options.add_experimental_option("excludeSwitches", ["enable-automation"])
                options.add_experimental_option('useAutomationExtension', False)
                driver = webdriver.Chrome(options=options)
                driver.maximize_window()
                driver.get('https://www.gmail.com')
              
                email_field = driver.find_element_by_css_selector(Locators.email_field_locator)
                email_field.send_keys(Config.sender_email)
            
                next_btn = driver.find_element_by_class_name(Locators.next_btn1)
                next_btn.click()
                time.sleep(5)
                password_field = driver.find_element_by_name(Locators.pass_field_locator)
                password_field.send_keys(Config.password)
                login_btn = driver.find_element_by_class_name(Locators.next_btn2)
                login_btn.click()
                time.sleep(5)
                compose_btn = driver.find_element_by_css_selector(Locators.compose_button)
                compose_btn.click()
                time.sleep(2)
                to_field = driver.find_element_by_name(Locators.send_to_field) 
                to_field.send_keys(Config.receiver_email)
                print('----Email address inputted----')
                time.sleep(2)
                subject_field = driver.find_element_by_name(Locators.subject_field)
                print('----Subject inputted----')
                subject_field.send_keys('subject test')
                body_field = driver.find_element_by_css_selector(Locators.mail_body)
                body_field.send_keys(Locators.mail_message)
                print('----Mail body inputted----')
                time.sleep(2)
                send_btn = driver.find_element_by_css_selector(".T-I.J-J5-Ji.aoO.v7.T-I-atl.L3")
                send_btn.click()
                send_btn.send_keys(Keys.CONTROL,Keys.RETURN)
                print('----Button send clicked----')
                driver.close()
            except Exception:
                    driver.save_screenshot('sreenshot.png')
               
    
if __name__ == '__main__':
    unittest.main()